#!/bin/bash

###############################################################
# OpenFoam 8 preparation script 

# This script should copy the folder structure for the different cases
# the purpose is to create a folder structure for a case study of a certain type of case where just the U file needs to be changed for each case

# by Arne Daniel

# 31.01.2023
# additionaly the script tries to rotate the angles and create different meshes for different angel of attacks for the sails.
# for now just 2mastdyna is implemented
# 03.03.2023
# this is the calulation for 3 masts!!!
# 22.05.2023 
# not anymore....

###############################################################

#set -o errexit #error is something went wrong


# send error message on error lol
# maybe set a new email address as receiver
#err_report() {
#  echo "Simulation crashed! Error $1 occurred on line $2" | mail -s "Simulation error!" arne.daniel@hs-emden-leer.de
#}

#trap 'err_report $? $LINENO' ERR


#check if source files are there
if [ ! -d "source_cases" ]; then
    echo "Source directory not found!"
    exit
fi

#check correct openFoamversion
if [ "$(dpkg -l | grep openfoam8 | awk '{print $2}')" != "openfoam8" ]; then
  echo "OpenFoam is not installed"
  exit
else
  echo "OpenFoam 8 installed. Continue..."
fi

# prevent to simulate if the amount of values of input fixed values is not the same
# testnumbers () {

#         if [ ${#velocities[@]} != ${#angles[@]} ] || [ ${#aofasail2[@]} != ${#aofasail1[@]} ] || [ ${#angles[@]} != ${#aofasail3[@]} ] || [ ${#aofasail2[@]} != ${#velocities[@]} ]; then
#                 echo "Error - amount of numbers wrong"
#                 exit
#         fi
# }

gen_startmatrix () {

        list=0

        for (( c="$aofasail1_start"; c<="$aofasail1_end"; c += "$modi_aoa" ))
                do 
                aofasail1[list]=$c
                aofasail1[list+1]=$c
                list=$((list+2))
        done

        # cut the last element
        length=${#aofasail1[@]}
        unset "aofasail1[$((length-1))]"

        list=0
        for (( c="$aofasail2_start"; c<="$aofasail2_end"; c += "$modi_aoa" ))
                do
                        aofasail2[list]=$c
                        aofasail2[list+1]=$c

                        # skip the first element
                        if [ $list == 0 ]; then
                        list=$((list+1))
                        else
                        list=$((list+2))
                        fi
        done               
}

# This function will rotate the stl files to the by the iteration needed angle for the 2mastDyna case for now...
rotate_rigg () {

        right_angle="$(echo "$2-$1"| bc -l)"
        shift="$3"
        #echo "$right_angle" > "$longpath"/mesh/constant/triSurface/.sailangle
        # 1> /dev/null just output if error
        surfaceTransformPoints -translate "( $(echo "(-1)*${shift}"| bc -l) 0 0)" sail"$4".stl sail"$4".stl 1> /dev/null
        surfaceTransformPoints -translate "( $(echo "(-1)*${shift}"| bc -l) 0 0)" mast"$4".stl mast"$4".stl 1> /dev/null
       
	surfaceTransformPoints -yawPitchRoll "(${right_angle} 0 0)" sail"$4".stl sail"$4".stl 1> /dev/null
	surfaceTransformPoints -yawPitchRoll "(${right_angle} 0 0)" mast"$4".stl mast"$4".stl 1> /dev/null

        surfaceTransformPoints -translate "(${shift} 0 0)" sail"$4".stl sail"$4".stl 1> /dev/null
        surfaceTransformPoints -translate "(${shift} 0 0)" mast"$4".stl mast"$4".stl 1> /dev/null

	echo "stl sail $4 rotated"
}


generatemesh () {
        
        echo "Start meshing..."
        echo "running Surfacefeatures"
        surfaceFeatures  1> /dev/null # generate the outercontour out of stl
        sed -i "s/^numberOfSubdomains.*/numberOfSubdomains $noprocmesh;/g" "$longpath"/mesh/system/decomposeParDict # replace the no of proc for mesh
        
        runApplication decomposePar  || exit
        

        runParallel snappyHexMesh
        #mpirun -np "$noprocmesh" snappyHexMesh -parallel 
        runApplication reconstructParMesh -latestTime 
        echo "copy polyMesh to simple"
        cp -r 3/polyMesh ../simple/constant/ #|| exit #for now
        rm -r processor*
        echo "Meshing done"  
}

getforcevalue () {

    file_path="$longpath/simple/postProcessing/$1/0/forces.dat"
    #  Read the last line of the text file
    last_line=$(tail -n 1 "$file_path")

    # Create array with the words of the last line
    read -ra words <<< "$last_line"

    # Extract the second element (index 1)
    sec_element=${words[1]}

    # scientific notation
    formatted_number=$(printf "%f" "${sec_element:2}")
    # sum up all forces
    sum=$(echo "$previous+${formatted_number}"| bc -l) 
    echo "$formatted_number"
    previous=$sum
}

# here the forces in x will be saved of the individual case for later usage
cuebestsolution () {

previous=0
for element in "${nameofpatches[@]}"
do
    getforcevalue "$element"
done

}

readpatches () {
        # Extrahiere den gewünschten String aus der Datei und weise ihn einer Variable zu
        mapfile -t nameofpatches < <(grep "patches" "$sourcecasepath/simple/system/controlDict" | sed 's/patches (\(.*\));/\1/')
        # Entferne Leerzeichen aus den Array-Elementen
        for i in "${!nameofpatches[@]}"; do
        nameofpatches[i]=$(echo "${nameofpatches[i]}" | tr -d '[:space:]')
        done
}

# $1 is aoasail1, $2 is aoasail2
simulate () {

        cd "$mainpath" || exit
	longpath=$mainpath'/'$simuname'/v'_$velocities'/'$angles'deg/'$1'/'$2
        echo "$longpath"
        
        mkdir -p "$longpath"  #create folder
        
        cp -r "$sourcecasepath"/*  "$longpath"  #copy simplecase

        echo ""
        echo "source OpenFoam functions and using "
        foamDictionary -entry application -value "$longpath"/simple/system/controlDict
        echo ""
	
        u_y=$(echo "scale=10; -$velocities*s(($angles* 3.141592)/180)" | bc -l) # sin calculation
        u_x=$(echo "scale=10; -$velocities*c(($angles* 3.141592)/180)" | bc -l) # cos calculation

        # change of the velocity/direction of the wind
        sed -i "s/^internalField   uniform.*/internalField   uniform ($u_x $u_y 0);/g" "$longpath"/simple/0/U
        sed -i "s/^numberOfSubdomains.*/numberOfSubdomains $noproc;/g" "$longpath"/simple/system/decomposeParDict # replace the no of proc
        sed -i "s/^endTime.*/endTime       $endtimeofsimulation;/g" "$longpath"/simple/system/controlDict # right endTime
	
  
        echo "generating new mesh"
        #mesh calc and angle rotation
        echo "entering $longpath"
        cd "$longpath"/mesh/constant/triSurface || exit

        # input:angle of sail, angle of wind, movement to x=0 for roatation and number of sail
        rotate_rigg "$1" "$angles" "$shiftx_sail1" "1"
        rotate_rigg "$2" "$angles" "$shiftx_sail2" "2"
        #rotate_rigg "${aofasail3[i]}" "$angles" "$shiftx_sail3" "3"
        
        cd "$longpath"/mesh || exit
        generatemesh # have to generate the new mesh 
        cp -r "$longpath"/mesh/3/polyMesh "$longpath"/simple/constant/


	#fi


	
	cd "$longpath"/simple || exit # move to simulation path
        
        # add something like:
        #bash "$mainpath/convergencetest.sh" "$longpath"/simple/postProcessing/sail1/0/forces.dat &
        #."$mainpath"/convergencetest.sh "$longpath"/simple/postProcessing/sail1forces/0/forces.dat
        ./Allrun || exit  # run own allrunscript
        #pgrep -f "convergencetest.sh" | xargs kill -SIGTERM || true # kill convergence test in case
       
        #df -PH /dev/mapper/flu1-root # show the rest of the diskspace
        echo "case $velocities m/s with $angles $1 bow $2 stern degree done"

        # here the forces in x will be saved of the individual case for later usage with longpath
        cuebestsolution
        sumofforces+=("$sum") # add new value to the sumofforces to determine the best 
        echo "force sum is $sum"


}

# this function calculates the new sailangles based on forces data
# $1 is the 1st highest force place in array $2 is the secnd
# opti_sail_conf () {
        
#         # calculate the mid between the best performing angels for both sails
#         finalanglesail1=$(echo "(${aofasail1[$1]}+${aofasail1[$2]})*0.5"| bc -l) 
#         finalanglesail2=$(echo "(${aofasail2[$1]}+${aofasail2[$2]})*0.5"| bc -l)   
#         echo "$finalanglesail1 new degree for sail1"
#         echo "$finalanglesail2 new degree for sail2"
# }

opti_sail_conf () {
        
        finalanglesail1[0]=$(echo "(${aofasail1[$1]})"| bc -l) # normal
        finalanglesail1[1]=$(echo "(${aofasail1[$1]})-1"| bc -l) 
        finalanglesail1[2]=$(echo "(${aofasail1[$1]})+1"| bc -l) 
        finalanglesail2[0]=$(echo "(${aofasail2[$1]})"| bc -l)   # normal
        finalanglesail2[1]=$(echo "(${aofasail2[$1]})-1"| bc -l)   
        finalanglesail2[2]=$(echo "(${aofasail2[$1]})+1"| bc -l)   

        echo "new approach is ${finalanglesail1[*]} and ${finalanglesail2[*]}"
}


# get the current best ratio between the angle of sail1 to sail2 developed by ratio of the 
# two max force values

get_sailangleratio () {

        local ratio1=0
        local ratio2=0
        ratio1=$(echo "(${aofasail2[$1]}/${aofasail1[$1]})"| bc -l) 
        ratio2=$(echo "(${aofasail2[$2]}/${aofasail1[$2]})"| bc -l) 
        opt_sail_ratio=$(echo "($ratio1+$ratio2)*0.5"| bc -l) 
        echo "current best sail ratio is $opt_sail_ratio"
}

# apply the estimated best sail ratio  to the sail2 angle at best place
set_sailangleratio () {
        aofasail2[$1]=$(echo "${aofasail1[$1]}*${opt_sail_ratio}"| bc -l) 
}

quit () {
        #echo "DEBUG: Nice is $nice"
        #echo "DEBUG: prev in quit is $highest1place_prev"
        if (( highest1 <= highest1place_prev )); then
                echo "already found best value"
                echo "see above - exiting"
                exit
        else
                echo "next calc necessary" 
        fi

        # quit, when new value has not reached a given percentage of the previous value
        local valuemustreach
        valuemustreach=$(echo "($needed_perc*0.01*$highest1place_prev)+$highest1place_prev" | bc -l)
        valuemustreach=${valuemustreach%.*} # turn to int
        if  (( highest1 <= valuemustreach )); then
        echo "final value found"
        echo "new value did not exceed $needed_perc from last maximum ($highest1place_prev)"
        exit
        fi
}
find_two_highest_values() {
    # Array als Parameter übergeben
    local values=("$@")
    
    # Variablen für die beiden höchsten Werte initialisieren
    local highest1=0
    local highest2=0
    local n=0
    highest1place=0
    highest2place=0
    # Iteration durch das Array
    for value in "${values[@]}"; do
    value=${value%.*} # make float to int (should be precise enought)
        if (( value > highest1 )); then
            highest2=$highest1
            highest2place=$highest1place
            highest1=$value
            highest1place=$n
        elif (( value > highest2 )); then
            highest2=$value
            highest2place=$n
        fi
        n=$(( "$n"+1 ))
    done

    # Ausgabe der beiden höchsten Werte
    echo "Höchster Wert: $highest1"
    echo "Zweithöchster Wert: $highest2"
    echo "highestplace 1 is $highest1place"
    echo "highestplace 2 is $highest2place"
    echo "case ist zwischen  sail 1/2 = ${aofasail1[$highest1place]}/${aofasail2[$highest1place]} "
    echo "und sail 1/2 = ${aofasail1[$highest2place]}/${aofasail2[$highest2place]} "

    # quit if already found best value even after optimization
    quit 
    echo "previous highest value is $highest1place_prev"
    highest1place_prev="$highest1" # save value for the next round
}

simulatearound () {
        simulate "${finalanglesail1[0]}" "${finalanglesail2[1]}"
        aofasail1+=("${finalanglesail1[0]}")
        aofasail2+=("${finalanglesail2[1]}")
        simulate "${finalanglesail1[0]}" "${finalanglesail2[2]}"
        aofasail1+=("${finalanglesail1[0]}")
        aofasail2+=("${finalanglesail2[2]}")        
        simulate "${finalanglesail1[1]}" "${finalanglesail2[0]}"
        aofasail1+=("${finalanglesail1[1]}")
        aofasail2+=("${finalanglesail2[0]}")      
        simulate "${finalanglesail1[2]}" "${finalanglesail2[0]}"
        aofasail1+=("${finalanglesail1[2]}")
        aofasail2+=("${finalanglesail2[0]}")      
}

#################### FIXED VALUES BY USER ########################
# chooice of the mesh:
simuname='test'
#case='3MastWing'
noproc=40 # no of processor for simple
noprocmesh=40 # no processor for meshing 
endtimeofsimulation=500 # end time of the simulation
sourcecasepath='/home/daniel/innocalc/source_cases/2MastDyna'





# needed velocity and wind angle 
# select the boundaries of the case in between a optimal value should be found
# select a fix velocity and a fix wind angle

velocities=10
angles=30
aofasail1_start=10 
aofasail2_start=10 
aofasail1_end=30 
aofasail2_end=30 
#aofasail3=( 10 40)
# shiftx_sail1=(21.794)         
# shiftx_sail2=(-0.8036) # measured in Paraview        
#shiftx_sail3=(-23.5026) # measured in Paraview     
shiftx_sail1=(14.9598) 
shiftx_sail2=(-19.5322)

modi_aoa=5

extendedsimus=3

# define a percentage to quit the simulation, when the previous max value times the percentage
# was not exceeded 
needed_perc=1



######################################################################

# shellcheck disable=SC1091
source /opt/openfoam8/etc/bashrc   # source OF8
. $WM_PROJECT_DIR/bin/tools/RunFunctions
# still in code but depreacted due to new approach
  #mkdir -p $case/v{3,6,9,12}/{20,25,30,40,50,60,80,100}deg #create all folders for veleocity and degree

#read out the patch names out ofn the forces definition in the controldict of source
readpatches

mainpath="$sourcecasepath"/../.. # the "root" directory

# generate the matrix of necessary simulations
gen_startmatrix 

#first loop for the "brute force" approach
for (( actualstate=0; actualstate<=(${#aofasail2[@]}-1); actualstate++ ))
do 
   simulate "${aofasail1[actualstate]}" "${aofasail2[actualstate]}"
   longpath=$mainpath'/'$simuname'/v'_$velocities'/'$angles'deg/'"${aofasail1[actualstate]}"'/'"${aofasail2[actualstate]}"

done
seconditermax=${#aofasail2[@]}-1
# execute $extendedsimus times more with auto approach
# take the last "actualstate" as begining
for (( actualstate; actualstate<=($seconditermax+"$extendedsimus"); actualstate++ ))
do 
        # read the best forces for sailangleconfiguration, is setting the needed
        # $highest1place and $highest2place
        echo "sum of forces is"
        echo "${sumofforces[@]}"
        echo " "
        find_two_highest_values "${sumofforces[@]}"

        echo "#### DEBUG actualstate is $actualstate ######"
        echo "#### DEBUG aoasail2 all is ${#aofasail2[@]} #####"
        echo "#### DEBUG extenimus is $extendedsimus #####"
        echo "#### DEBUG seconditermax is $seconditermax #####"


        # calculate "around" the best value, input is the number of placement in array
        opti_sail_conf "$highest1place"

        #do simulations around the best force value siumlation
        simulatearound


done

echo "All done "


######## TODO #######
