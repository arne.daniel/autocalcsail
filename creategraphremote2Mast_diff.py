import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as plt
import datetime  
from matplotlib import dates as mpl_dates
import statistics
import glob
import os 
import sys
import math
import pathlib

os.chdir(".")


################# CARE ###########
# This script was turned to remote read!!!

##########################################


################# SETTINGS BY USER #################
#case ='2MastDyna'
case1='/home/daniel/innosailer/2MastDynaref1'
case2='/media/secondPartition/daniel/innosailcalc/2MastDyna'
case3='/home/daniel/innosailer/2MastDynaref2'
case1sailsurface= ((  2340.1/2  ))
case2sailsurface= (( 2487.6/2 ))
case3sailsurface= (( 2107.1/2 ))
hulllateralsurface= 538
host1='flu3.hs-el.de'#
host2='flu4.hs-el.de'
host3='flu3.hs-el.de'
user='daniel'
casesprobed=7

rotated = False #true or false
cutvales = 0 # how many values to cutin front
sternforces_switch = False
bowforces_switch = False
sumforces_switch = True
addhull_switch = True

norm= True # should be normalized

####################################################


from fabric.connection import Connection





bowforcedata = []
sternforcedata = []
hullforcedata =[]
# filter out velocity and apparent wind angle
#print(bowforcedata[2].split("/")[6][2], bowforcedata[1].split("/")[7][0:2] )
# initilisation
simutime = {}
force_x_bow = {}
force_y_bow = {}
force_x_stern = {}
force_y_stern = {}
force_x_sum = {}
force_y_sum = {}
caselist = {}
awslist = []
awalist = []
aoabowlist = []
aoasternlist = []

# function for koordinate transforation
# 360 for turning the system
def rotatex(valuex, valuey):
    #angle = (360 - angle)
    angle= int(awa)
    preventwrongdata(valuex, valuey)
    for entry in range(len(valuex)):
        valuey[entry]= (((-1*valuex[entry]) * math.sin(math.radians(angle)) + (valuey[entry] * math.cos(math.radians(angle)))))
        valuex[entry]= ((valuex[entry] * math.cos(math.radians(angle)) + (valuey[entry] * math.sin(math.radians(angle)))))
    return valuex, valuey

### Prevents wrong data by comparing the length of the list
def preventwrongdata(x, y):
    if len(x) != len(y):
        print("Error - data is wrong")
        exit()

### normalises the forcevalues with surface and fluid speed and density
def normi (convunit, surface_normi):
    v_normi= awslist[-1]
    roh_norm=1.2041 #kg/m^3
    for entry in range(len(convunit)):
        convunit[entry]= abs((( convunit[entry]/(0.5*roh_norm*math.pow(v_normi, 2)*surface_normi) )))
    return convunit


#clean up a value from the brackets and tranform from scientific to float 
def cleanvalues(value):
    value= str(value.replace("((", ""))
    value= float(value)
    return value
 

def writetolist(fx, fy, time):
    for entry in range(len(fx)):
        fx[entry] = cleanvalues(fx[entry])
        fy[entry] = cleanvalues(fy[entry])
        time[entry]=int(time[entry])
    return fx, fy, time
 

def sumvalues(value1, value2, value3):
    sumvalue = []
    for entry in range(len(value1)):
        if addhull_switch:
            pass
        else:
            value3[entry]=0
      
        sumvalue.append(value1[entry] + value2[entry] + value3[entry] ) 
    return sumvalue

# def localwrite(data):
#     ii = 0
#     time = []
#     fx = []
#     fy = []
#     for line in open(data, 'r'):
#         if ii <= 2: # prevent that the first few lines will be read
#             ii = ii + 1
#             continue
#         lines = [i for i in line.split()]
#         time.append((lines[0]))
#         fx.append((lines[1]))
#         fy.append((lines[2])) 
#     return (fx, fy, time)

def remotewrite(data, remotehost):
    ii = 0
    time = []
    fx = []
    fy = []
    with Connection(remotehost, user) as c, c.sftp() as sftp,   \
            sftp.open(data) as filee:
        for line in filee:
 
            if ii <= 2: # prevent that the first few lines will be read
                ii = ii + 1
                continue
            lines = [i for i in line.split()]
            time.append((lines[0]))
            fx.append((lines[1]))
            fy.append((lines[2])) 
    return (fx, fy, time)   

def remotewritelastvalue(data, remotehost):
    ii = 0
    time = []
    fx = []
    fy = []
    with Connection(remotehost, user) as c, c.sftp() as sftp,   \
            sftp.open(data) as filee:
        last_line = filee.readlines()[-1]
        lines = [i for i in last_line.split()]
        time.append((lines[0]))
        fx.append((lines[1]))
        fy.append((lines[2])) 
    return (fx, fy, time) 

def remotedataread(remotehost, case):
    with Connection(remotehost, user) as c, c.sftp() as sftp:
            output= c.run('find '+case+' -type f -name forces.dat', warn=True)
            output= output.stdout.split("\n")[:-1]
            

            for iii in range(len(output)):
                if "bowsailforces" in output[iii]:
                    bowforcedata.append(output[iii])
                elif "sternsailforces" in output[iii]:
                    sternforcedata.append(output[iii])
                elif "hullforces" in output[iii]:
                    hullforcedata.append(output[iii])
            c.close()


def clearvars():
    bowforcedata.clear()
    sternforcedata.clear()
    hullforcedata.clear()
    simutime.clear()
    force_x_bow.clear()
    force_y_bow.clear()
    force_x_stern.clear()
    force_y_stern.clear()
    force_x_sum.clear()
    force_y_sum.clear()
    caselist.clear()
    awslist.clear()
    awalist.clear()
    aoabowlist.clear()
    aoasternlist.clear()


def mainrun(rangee, host):
    
    # Check if the files have the same length
    preventwrongdata(bowforcedata, sternforcedata)

    for file in range(rangee):
        
        time_bow = []
        fx_bow = []
        fy_bow = []
        time_stern = []
        fx_stern = []
        fy_stern = []  
        time_hull = []
        fx_hull = []
        fy_hull = []  
        fx_sum = []
        fy_sum= []
        
        degbow = [k for k in bowforcedata[file].split("/") if 'degbow' in k][0][:-6]
        degstern = [k for k in bowforcedata[file].split("/") if 'degstern' in k][0][:-8]
        vel = [k for k in bowforcedata[file].split("/") if 'v_' in k][0][2:]
        global awa
        awa = [k for k in bowforcedata[file].split("/") if 'deg' in k][0][:-3]
        awslist.append(int(vel))
        awalist.append(int(awa))
        aoabowlist.append(int(degbow))
        aoasternlist.append(int(degstern))
    
        # single case name define
        caselist[file] = ("vel"+vel+"_awa"+awa+"_degbow"+degbow+"_degstern"+degstern)

        # initial write out of data
        fx_bow, fy_bow, time_bow = remotewritelastvalue(bowforcedata[file], host)
        fx_stern, fy_stern, time_stern = remotewritelastvalue(sternforcedata[file], host)
        fx_hull, fy_hull, time_hull = remotewritelastvalue(hullforcedata[file], host)
        
        # conversion and tranforming the data
        writetolist(fx_bow, fy_bow, time_bow)
        writetolist(fx_stern, fy_stern, time_stern)
        writetolist(fx_hull, fy_hull, time_hull)
        #if sumforces_switch:
        fx_sum = sumvalues(fx_bow, fx_stern, fx_hull)
        fy_sum = sumvalues(fy_bow, fy_stern, fy_hull)


        if rotated:
            rotatex(fx_bow, fy_bow)
            rotatex(fx_stern, fy_stern)
            rotatex(fx_sum, fy_sum)        
        if norm:
            for count in (fx_bow, fy_bow, fx_stern, fy_stern):
                count= normi(count, surface)
            for count_sum in (fx_sum, fy_sum):
                count_sum= normi(count_sum, (((2*surface))+hulllateralsurface))
        force_x_bow[file]= fx_bow
        force_y_bow[file]= fy_bow
        force_x_stern[file]= fx_stern
        force_y_stern[file]= fy_stern
        force_x_sum[file]= fx_sum     
        force_y_sum[file]= fy_sum 
        
def plotpolar(vel, windangle, farbe, label_plot):
    n=0 # counter for legend

    def doplot(variant):
        plt.legend(loc="best")
        plt.grid(True,linestyle = '--')
        plt.annotate('ab=aoabowsail\nas=aoasternsail', xy=(0.2, 0.5),xycoords='axes fraction', fontsize=8, horizontalalignment='right', \
         verticalalignment='bottom', bbox=dict(facecolor='white'))        
        plt.title(variant+"x/"+variant+"y "+str(vel)+" m/s - awa="+str(windangle)+"°")
        if variant == 'C':
            plt.ylabel(variant+'y')
            plt.xlabel(variant+'x')
        else:
            plt.ylabel(variant+'y in N')
            plt.xlabel(variant+'x in N')
        plt.savefig('/home/daniel/innocalc/plots/2MastDyna_diffref/'+variant+'x_'+variant+'y_'+str(vel)+"_ms_"+str(windangle)+"°", \
            dpi=500,bbox_inches='tight')
    


    for caseno in range(len(caselist)):
        if awslist[caseno] == vel:
                if abs(force_x_sum[caseno][0]) <= 20000000: # filter obviously wrong cases!
                    if (awalist[caseno]) == windangle:
                        if n == 0:
                            plt.plot(force_x_sum[caseno][0], force_y_sum[caseno][0], '-ok', color=farbe, label=label_plot)
                            n=1
                        else:
                            plt.plot(force_x_sum[caseno][0], force_y_sum[caseno][0], '-ok', color=farbe)
                        
                        plt.text(force_x_sum[caseno][0]+(force_x_sum[caseno][0]/300), force_y_sum[caseno][0], 
                                " ab:"+str(aoabowlist[caseno])
                                +" as:"+str(aoasternlist[caseno]),fontsize=5)
    
    if norm:
        doplot('C')
    else:
        doplot('F')

def makesimpletimeforcesplot(which,rota_true):
    fig,ax=plt.subplots()
    timeplot = simutime[which][cutvales:]
    forcexplot_bow = force_x_bow[which][cutvales:]
    forceyplot_bow = force_y_bow[which][cutvales:]
    forcexplot_stern = force_x_stern[which][cutvales:]
    forceyplot_stern = force_y_stern[which][cutvales:]
    if sternforces_switch:
        ax.plot(timeplot, forcexplot_stern, color='salmon',  label='F_x_stern')
    if bowforces_switch:
        ax.plot(timeplot, forcexplot_bow, color='r',  label='F_x_bow')    
    if sumforces_switch:
        forcexsum_plot = force_x_sum[which][cutvales:]
        forceysum_plot = force_y_sum[which][cutvales:]
        ax.plot(timeplot, forcexsum_plot, color='sienna',  label='F_x_both')    

    
    ax.set_ylabel('Fx in N', color="red")
    # plt.text(int(len(timeplot)*0.9), int(max(forcexplot_bow)*0.5), statistics.median(forcexplot_bow),
    #     horizontalalignment='center',
    #     verticalalignment='center')
    ax.set_title(caselist[which])

    # prevent error when no data with shrinkage
    if len(timeplot)/10 == 0:
        ax.text(4, 0, 'data not valid ', fontsize = 22, bbox = dict(facecolor = 'red', alpha = 0.5))
        ax.set_xticks(simutime[which], rotation=90)
    else:
        #ax.set_xticks(timeplot[::int(len(timeplot)/10)], rotation=90)
        print("nice")
    ax2=ax.twinx() # second axis
    if sternforces_switch:
        ax2.plot(timeplot, forceyplot_stern, color='lime', label='F_y_stern')
    if bowforces_switch:
        ax2.plot(timeplot, forceyplot_bow, color='g', label='F_y_bow')
    if sumforces_switch:
        ax2.plot(timeplot, forceysum_plot, color='aquamarine', label='F_y_both')


    ax2.set_ylabel('Fy in N', color="green")  

    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(lines + lines2, labels + labels2, loc='upper left')
    
    #plt.gcf().autofmt_xdate()
    #name_plot = "graph" + str(which) + ".svg" #LATE for good SVG
    #name_plot = "graph" + str(which) + ".png"
    #plt.savefig(name_plot)
    plt.grid()
    if rota_true== 'true':
        plt.savefig('plots/'+case+'rotated/'+caselist[which], dpi=300,bbox_inches='tight')
    else:
        plt.savefig('plots/'+case+'/'+caselist[which], dpi=300,bbox_inches='tight')#
    plt.close()

# for i in range(len(caselist)):
#     makesimpletimeforcesplot(i,rotated)



surface=case2sailsurface 
remotedataread(host2, case2)
mainrun(len(bowforcedata), host2)
plotpolar(10, 30,'g', case2.split("/")[-1])

clearvars()

surface=case1sailsurface # set the right surface
remotedataread(host1, case1)
mainrun((casesprobed-1), host1)
plotpolar(10, 30,'r', case1.split("/")[-1])

clearvars()
surface=case3sailsurface 
remotedataread(host3, case3)
mainrun((casesprobed-1), host3)
plotpolar(10, 30,'orange', case3.split("/")[-1])
#makesimpletimeforcesplot(5,rotated)

