#!/bin/bash
aofasail1_start=10
aofasail2_start=10
modi_aoa=5
aofasail1_end=30
aofasail2_end=30
list=0
aofasail1=()

for (( c=aofasail1_start; c<="$aofasail1_end"; c += "$modi_aoa" ))
do 
    aofasail1[list]=$c
    aofasail1[list+1]=$c

    list=$((list+2))
done


length=${#aofasail1[@]}
unset "aofasail1[$((length-1))]"
# echo "aoa sail1"
# echo "${aofasail1[@]}"

list=0
for (( c=aofasail2_start; c<="$aofasail2_end"; c += "$modi_aoa" ))
do 

    aofasail2[list]=$c
    aofasail2[list+1]=$c
    if [ $list == 0 ]; then
    list=$((list+1))
    else
    list=$((list+2))
    fi
   
done
# echo "aoa sail2"
# echo "${aofasail2[@]}" 


find_two_highest_values() {
    # Array als Parameter übergeben
    local values=("$@")
    
    # Variablen für die beiden höchsten Werte initialisieren
    local highest1=0
    local highest2=0
    local n=0
    highest1place=0
    highest2place=0
    # Iteration durch das Array
    for value in "${values[@]}"; do
        if (( value > highest1 )); then
            highest2=$highest1
            highest1=$value
            highest1place=$n
        elif (( value > highest2 )); then
            highest2=$value
            highest2place=$n

        fi
        n=$(( "$n"+1 ))
    done

    # Ausgabe der beiden höchsten Werte
    # echo "Höchster Wert: $highest1"
    # echo "Zweithöchster Wert: $highest2"
    # echo "highestplace 1 is $highest1place"
    # echo "highestplace 2 is $highest2place"
}

# Array von Werten
my_array=(15 500 100 30  10  20 )

# Aufruf der Funktion mit dem Array als Argument
find_two_highest_values "${my_array[@]}"
finalanglesail1[0]=$(echo "(${aofasail1[$highest1place]}+${aofasail1[$highest2place]})*0.5"| bc -l) 
finalanglesail1[1]=$(echo "(${aofasail1[$highest1place]}+${aofasail1[$highest2place]})*0.5"| bc -l) 
#echo "highest angle is ${aofasail1[$highest1place]}"
#echo "second angle is ${aofasail1[$highest2place]}"
#echo "${finalanglesail1[@]}"

# Dateipfad zur Textdatei
dateipfad="/home/daniel/innocalc/source_cases/2MastDyna/simple/system/controlDict"

# Extrahiere den gewünschten String aus der Datei und weise ihn einer Variable zu
mapfile -t array < <(grep "patches" "$dateipfad" | sed 's/patches (\(.*\));/\1/')
# Entferne Leerzeichen aus den Array-Elementen
for i in "${!array[@]}"; do
  array[i]=$(echo "${array[i]}" | tr -d '[:space:]')
done
#echo "${array[1]}"
highest1place_prev="$highest1place" # backup for next round

# anglesss=$(echo "(${aofasail2[$highest1place]}/${aofasail1[$highest1place]})"| bc -l) 
# echo "$anglesss"

if (( highest1place != highest1place_prev)); then
    if (( highest1place_prev != 0 )); then
    echo "nice"
    fi
fi

quit () {

    local valuefirst=1000
    local valuelast=1010
    needed_perc=2
    local valuemustreach=$(echo "($needed_perc*0.01*$valuefirst)+$valuefirst" | bc -l)
    valuemustreach=${valuemustreach%.*}
    if  (( valuelast <= valuemustreach )); then
        echo "final value found"
        echo "new value did not exceed $needed_perc from last maximum ($valuefirst)"
        exit
    fi
}
quit