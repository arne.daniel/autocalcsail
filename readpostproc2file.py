#!/usr/bin/env python

""" 
    Creator: Arne Daniel
    Date: 07.07.2023
    Description: This script reads all postProcessing data of a underlaying 
    folder structure of OpenFoam and writes all to one file for further 
    investigation
    
"""

import os
import re
import csv
########### USER ############

filespath = '.'
filename = 'forces.dat'
savefile = 'output.csv'
patch1 = 'sail1'
patch2 = 'sail2'
patch3 = 'hull'

##############################

#clean up a value from the brackets and tranform from scientific to float
def cleanvalues(value):
    value= str(value.replace("((", ""))
    value= float(value)
    return value

def search_files(path, filename):
    found_paths = []  # Liste zur Speicherung der gefundenen Dateipfade
    for root, dirs, files in os.walk(path):
        for file in files:
            if file == filename:
                file_path = os.path.join(root, file)
                found_paths.append(file_path)
    return found_paths

def writeheaderandclean():
    with open(savefile, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['patchname', 'windspeed[m/s]', 'windangle[deg]', \
            'sail1angle[deg]', 'sail2angle[deg]', 'force_x[N]'])

def writeto_file(data1, data2, data3, data4, data5, data6):
    # Öffne die CSV-Datei im Schreibmodus
    with open(savefile, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)

        writer.writerow([data1, data2, data3, data4, data5, data6])



forcefile_paths = search_files(filespath, filename)
forces_list = []  # Liste zur Speicherung der Daten

if forcefile_paths:
    # Dateien gefunden
    writeheaderandclean()
    for file_path in forcefile_paths:
        with open(file_path, 'r') as file:
            # Aktionen mit der geöffneten Datei durchführen
            lines = file.readlines()
            last_line = lines[-1].strip()
            elements = last_line.split()
            match = re.search(r'deg/(\d+)/(\d+)', file_path) # match deg values behind "deg"
            sail1deg = match.group(1)
            sail2deg = match.group(2)
            vel = [k for k in file_path.split("/") if 'v_' in k][0][2:]
            patchname=''
            for run in file_path.split("/"): #get the patch names
                if patch1 in run:
                    patchname = patch1
                elif patch2 in run:
                    patchname = patch2
                elif patch3 in run:
                    patchname = patch3
            if patchname == '':
                raise Exception("Patchname not found!\nPlease check the name of the defined patches") 
            awa = [k for k in file_path.split("/") if 'deg' in k][0][:-3]
            if len(elements) >= 2:
                writeto_file(patchname, vel, awa, sail1deg, sail2deg, cleanvalues(elements[1]))
            
else:
    # Dateien nicht gefunden
    print("Die Dateien wurden nicht gefunden.")