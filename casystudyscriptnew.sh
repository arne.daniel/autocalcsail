#!/bin/bash

###############################################################
# OpenFoam 8 preparation script 

# This script should copy the folder structure for the different cases
# the purpose is to create a folder structure for a case study of a certain type of case where just the U file needs to be changed for each case

# by Arne Daniel

# 31.01.2023
# additionaly the script tries to rotate the angles and create different meshes for different angel of attacks for the sails.
# for now just 2mastdyna is implemented
# 03.03.2023
# this is the calulation for 3 masts!!!

###############################################################

#set -o errexit #error is something went wrong


# send error message on error lol
# maybe set a new email address as receiver
#err_report() {
#  echo "Simulation crashed! Error $1 occurred on line $2" | mail -s "Simulation error!" arne.daniel@hs-emden-leer.de
#}

#trap 'err_report $? $LINENO' ERR


#check if source files are there
if [ ! -d "source_cases" ]; then
    echo "Source directory not found!"
    exit
fi

#check correct openFoamversion
if [ "$(dpkg -l | grep openfoam8 | awk '{print $2}')" != "openfoam8" ]; then
  echo "OpenFoam is not installed"
  exit
else
  echo "OpenFoam 8 installed. Continue..."
fi

# prevent to simulate if the amount of values of input fixed values is not the same
testnumbers () {

        if [ ${#velocities[@]} != ${#angles[@]} ] || [ ${#aofasail2[@]} != ${#aofasail1[@]} ] || [ ${#angles[@]} != ${#aofasail3[@]} ] || [ ${#aofasail2[@]} != ${#velocities[@]} ]; then
                echo "Error - amount of numbers wrong"
                exit
        fi
}


# This function will rotate the stl files to the by the iteration needed angle for the 2mastDyna case for now...
rotate_rigg () {

        right_angle="$(echo "$2-$1"| bc -l)"
        shift="$3"
        #echo "$right_angle" > "$longpath"/mesh/constant/triSurface/.sailangle
        # 1> /dev/null just output if error
        surfaceTransformPoints -translate "( $(echo "(-1)*${shift}"| bc -l) 0 0)" sail"$4".stl sail"$4".stl 1> /dev/null
        surfaceTransformPoints -translate "( $(echo "(-1)*${shift}"| bc -l) 0 0)" mast"$4".stl mast"$4".stl 1> /dev/null
       
	surfaceTransformPoints -yawPitchRoll "(${right_angle} 0 0)" sail"$4".stl sail"$4".stl 1> /dev/null
	surfaceTransformPoints -yawPitchRoll "(${right_angle} 0 0)" mast"$4".stl mast"$4".stl 1> /dev/null

        surfaceTransformPoints -translate "(${shift} 0 0)" sail"$4".stl sail"$4".stl 1> /dev/null
        surfaceTransformPoints -translate "(${shift} 0 0)" mast"$4".stl mast"$4".stl 1> /dev/null

	echo "stl sail $4 rotated"
}


generatemesh () {
        echo "Start meshing..."
        echo "running Surfacefeatures"
        surfaceFeatures  1> /dev/null # generate the outercontour out of stl
        sed -i "s/^numberOfSubdomains.*/numberOfSubdomains $noprocmesh;/g" "$longpath"/mesh/system/decomposeParDict # replace the no of proc for mesh
        
        runApplication decomposePar  || exit
        

        runParallel snappyHexMesh
        #mpirun -np "$noprocmesh" snappyHexMesh -parallel 
        runApplication reconstructParMesh -latestTime 
        echo "copy polyMesh to simple"
        cp -r 3/polyMesh ../simple/constant/ || exit
        rm -r processor*
        echo "Meshing done"
       
}

# calulatedcheck () {
#         if [ -d "$(find "$mainpath"/"$case" -path "*v_$1*$2deg*$3degbow*$4degstern*" -print -quit)" ]; then
#                 if [ "$(find "$mainpath"/"$case" -path "*v_$1*$2deg*$3degbow*$4degstern*" -print -quit)" == "$longpath" ]; then
#                         skip=1
#                         echo "we can skip next"
#                 fi
#                 skip=0
#         else
#                 skip=0
#         fi
        
# }

#################### FIXED VALUES BY USER ########################
# chooice of the mesh:

case='3MastDyna'
#case='3MastWing'
noproc=40 # no of processor for simple
noprocmesh=40 # no processor for meshing 
endtimeofsimulation=500 # end time of the simulation

echo 'Case chosen:'
echo $case

################################################################


# needed velocity and wind angle 

if [ "$case" = '3MastWing' ]; then
        velocities=(10 10 10 10 10 10 10 10 10 5 5 5 10 10 10 10 10 10 10 10 10)
        angles=(30 30 30  30 30 30  30 30 30  30 30 30  60 60 90 90 90 90 135 135 180)
        aofabow=(5 5 5 10 10 10 15 15 15 10 10 10 10 10 10 10 20 20 10 10 90)
        aofastern=(5 10 15 10 15 20 15 20 25 10 15 20 10 20 10 20 20 30 10 20 90)
        # shift of sails to (0 0 0) for rotation. This value represents the x mid of the sail in width
        # shiftx_bow=(14.9598) 
        # shiftx_stern=(-19.5322)
elif [ "$case" = '3MastDyna' ]; then
        # velocities=( 10 10 10 10 10 10 10 10 10 10 10)
        # angles=( 30 30 30 30 30 60 60 90 90 90 90)
        # aofasail1=( 10 10 15 15 15 10 10 10 10 20 20)
        # aofasail2=( 15 20 15 20 25 15 20 10 20 20 30)
        # aofasail3=( 20 30 15 25 35 20 30 10 30 20 40)
        velocities=(  10 )
        angles=( 90 )
        aofasail1=(   20 )
        aofasail2=(   30 )
        aofasail3=(   40  )
        shiftx_sail1=(21.794)         
        shiftx_sail2=(-0.8036) # measured in Paraview        
        shiftx_sail3=(-23.5026) # measured in Paraview        
else
        echo "error - case not found"
        exit
fi


testnumbers

######################################################################

# shellcheck disable=SC1091
source /opt/openfoam8/etc/bashrc   # source OF8
. $WM_PROJECT_DIR/bin/tools/RunFunctions
# still in code but depreacted due to new approach
  #mkdir -p $case/v{3,6,9,12}/{20,25,30,40,50,60,80,100}deg #create all folders for veleocity and degree


# create all the folders...

mainpath=$(pwd)  #declaring the main path

for ((i=0; i<=(${#velocities[@]}-1); i++));do  
        
        cd "$mainpath" || exit
	longpath=$mainpath'/'$case"/v"_${velocities[i]}'/'${angles[i]}'deg/'${aofasail1[i]}'/'${aofasail2[i]}'/'${aofasail3[i]}

        # simulation done before?
        # calulatedcheck "${velocities[i]}" "${angles[i]}" "${aofabow[i]}" "${aofastern[i]}"
        # if [ $skip == 1 ]; then
        #         continue
        # fi

        mkdir -p "$longpath"  #create folder
        cp -r "$mainpath"/source_cases/$case/*  "$longpath"  #copy simplecase

        echo ""
        echo "source OpenFoam functions and using "
        foamDictionary -entry application -value "$longpath"/simple/system/controlDict
        echo ""
	
        u_y=$(echo "scale=10; -${velocities[i]}*s((${angles[i]}* 3.141592)/180)" | bc -l) # sin calculation
        u_x=$(echo "scale=10; -${velocities[i]}*c((${angles[i]}* 3.141592)/180)" | bc -l) # cos calculation

        # change of the velocity/direction of the wind
        sed -i "s/^internalField   uniform.*/internalField   uniform ($u_x $u_y 0);/g" "$longpath"/simple/0/U
        sed -i "s/^numberOfSubdomains.*/numberOfSubdomains $noproc;/g" "$longpath"/simple/system/decomposeParDict # replace the no of proc
        sed -i "s/^endTime.*/endTime       $endtimeofsimulation;/g" "$longpath"/simple/system/controlDict # right endTime
	
        # do not generate mesh if not necassary
        # the , searches for 2 names | -print-quit just takes the first result of the search
        if [ -d "$(find "$mainpath"/$case -path "*${angles[i]}deg/${aofasail1[i]}/${aofasail2[i]}/${aofasail3[i]}*mesh/3" -print -quit)" ]; # if folder with old mesh exists...
        then
                oldmeshpath=$(find "$mainpath"/$case -path "*${angles[i]}deg/${aofasail1[i]}/${aofasail2[i]}/${aofasail3[i]}*mesh/3" -print -quit)'/polyMesh'
                echo "Old matching mesh found!" 
                echo "Taking old mesh in $oldmeshpath" 
                cp -r "$oldmeshpath" "$longpath"/simple/constant || exit #copy old mesh to system
	else
   
                echo "generating new mesh"
                #mesh calc and angle rotation
                echo "entering $longpath"
	       	cd "$longpath"/mesh/constant/triSurface || exit

                # input:angle of sail, angle of wind, movement to x=0 for roatation and number of sail
                rotate_rigg "${aofasail1[i]}" "${angles[i]}" "$shiftx_sail1" "1"
                rotate_rigg "${aofasail2[i]}" "${angles[i]}" "$shiftx_sail2" "2"
                rotate_rigg "${aofasail3[i]}" "${angles[i]}" "$shiftx_sail3" "3"
                
		cd "$longpath"/mesh || exit
                generatemesh # have to generate the new mesh 
                cp -r "$longpath"/mesh/3/polyMesh "$longpath"/simple/constant/


	fi


	
	cd "$longpath"/simple || exit # move to simulation path
        
        # add something like:
        bash "$mainpath/convergencetest.sh" "$longpath"/simple/postProcessing/sail1/0/forces.dat &
        #."$mainpath"/convergencetest.sh "$longpath"/simple/postProcessing/sail1forces/0/forces.dat
        ./Allrun || exit  # run own allrunscript
        pgrep -f "convergencetest.sh" | xargs kill -SIGTERM || true # kill convergence test in case
       
        #df -PH /dev/mapper/flu1-root # show the rest of the diskspace
        echo "case ${velocities[i]} m/s with ${angles[i]} ${aofabow[i]}bow ${aofastern[i]}stern degree done"
done

echo "All done "


######## TODO #######
