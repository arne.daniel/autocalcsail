#!/bin/bash


valuestocut=20 # cut for the convergence criteria
percentabba=0.002 # percentage when abort simpleFoam due to convergence
datafilebow=$1

abortifconvergence () {
        
        while IFS=' ' read -a line;do
            forcex+=("$(echo "${line[1]:3}" | awk -F"E" 'BEGIN{OFMT="%10.10f"} {print $1 * (10 ^ $2)}')") # truncate to float value
            forcey+=("$(echo "${line[2]}" | awk -F"E" 'BEGIN{OFMT="%10.10f"} {print $1 * (10 ^ $2)}')")
            time+=("$(echo "${line[0]}" | awk -F"E" 'BEGIN{OFMT="%10.10f"} {print $1 * (10 ^ $2)}')")          
        done < "$datafilebow"
        
       
        lenx=${#forcex[@]} #starts at 0
        leny=${#forcey[@]} #starts at 0
        if [ "$lenx" -le $valuestocut ]; then
            echo "not enough time steps!"
            return 1
        fi
        lenminusx=$(echo "${lenx}"-$valuestocut | bc -l)
        lenminusy=$(echo "${leny}"-$valuestocut | bc -l)

        sumx=0 # initial 0
        sumy=0
        for ((i="$lenminusx"; i<=(${#forcex[@]}-1); i++));do            
            sumx=$(echo "$sumx+${forcex[i]}" | bc -l)
        done
        for ((i="$lenminusy"; i<=(${#forcey[@]}-1); i++));do            
            sumy=$(echo "$sumy+${forcey[i]}" | bc -l)
        done    

        #echo "${forcex[@]}"
        meanx=$(echo "$sumx/$valuestocut" | bc -l) # calculate the arithetic mean
        meany=$(echo "$sumy/$valuestocut" | bc -l) # calculate the arithetic mean

        percx=$(echo "$meanx*$percentabba" | bc -l)
        percy=$(echo "$meany*$percentabba" | bc -l)
        percx=${percx#-} # make it to absolute value
        percy=${percy#-} # make it to absolute value
        echo "$percentabba of meanxvalue is $percx"
        echo "$percentabba of meanyvalue is $percy"

   
        deltavalx=$(echo "(${forcex[$lenminusx]})-(${forcex[$lenx-1]})" | bc -l) # delta between last and last-valuestocut
        deltavaly=$(echo "(${forcey[$lenminusy]})-(${forcey[$leny-1]})" | bc -l) # delta between last and last-valuestocut

        deltavalx=${deltavalx#-} # make it to absolute value
        deltavaly=${deltavaly#-} # make it to absolute value

        echo "delta ablsoute x value is ${deltavalx}"
        echo "delta ablsoute y value is ${deltavaly}"

        if (( $(echo "${deltavalx} < $percx" |bc -l) )); then
            if (( $(echo "${deltavaly} < $percy" |bc -l) )); then
                echo "convergence"  
                pgrep -f "mpirun" | xargs kill -SIGTERM # kill mpirun process
                {
                    echo "success at ${time[-1]} !!!"
                    echo "for $datafilebow"
                    echo ""
                } >> convergences.log
                exit            
            fi
            else
                echo "no convergence"

        fi
}

#abortifconvergence

loopabortcount=0
while [ $loopabortcount -le 300 ]
do
    if pgrep -x "mpirun" >/dev/null
    then
        abortifconvergence
    fi
    echo "sleep for 60 s with $loopabortcount"
    sleep 60
    ((loopabortcount++))
done


