import matplotlib
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as plt
import datetime  
from matplotlib import dates as mpl_dates
import statistics
import glob
import os 
import sys
import math
import pathlib
import copy
os.chdir(".")


################# CARE ###########
# This script was turned to remote read!!!

##########################################


################# SETTINGS BY USER #################
case1='/home/daniel/innocalc/3MastDyna'
case2='/media/secondPartition/daniel/innosailcalc/2MastDyna'

# case1='/home/daniel/innosailcalc/3MastWing'
# case2='/home/daniel/innosailcalc/2MastWing'

# case1sailsurface= (( 1629.4/3 ))    #3mastwing
# case2sailsurface= (( 1642.4/2 )) # 2mastwing


case1sailsurface= (( 2484.2/3 ))  #3mastdyna
case2sailsurface= (( 2487.6/2 ))    #2mastdyna

hulllateralsurface= 538
# host1='calc1.lab'#
# host2='calc1.lab'

host1='calc2.lab'#
host2='flu4.hs-el.de'

# naming='3and2mastWing_diff'
naming='3and2mastDyna_diff'

#naming='3MastDyna'

user='daniel'

rotated = False #true or false
cutvales = 0 # how many values to cutin front
sternforces_switch = False
bowforces_switch = False
sumforces_switch = True
addhull_switch = True

norm= False # should be normalized

####################################################


from fabric.connection import Connection





sail1data = []
sail2data = []
sail3data = []
hullforcedata =[]
# filter out velocity and apparent wind angle
#print(sail1data[2].split("/")[6][2], sail1data[1].split("/")[7][0:2] )
# initilisation
simutime = {}
listforce_x_sail1 = {}
listforce_y_sail1 = {}
listforce_x_sail2 = {}
listforce_y_sail2 = {}
listforce_x_sail3 = {}
listforce_y_sail3 = {}
force_x_sum = {}
force_y_sum = {}
caselist = {}
awslist = []
awalist = []
aoasail1list = []
aoasail2list = []
aoasail3list = []

# function for koordinate transforation
# 360 for turning the system
def rotatex(valuex, valuey):
    #angle = (360 - angle)
    angle= int(awa)
    preventwrongdata(valuex, valuey)
    for entry in range(len(valuex)):
        valuey[entry]= (((-1*valuex[entry]) * math.sin(math.radians(angle)) + (valuey[entry] * math.cos(math.radians(angle)))))
        valuex[entry]= ((valuex[entry] * math.cos(math.radians(angle)) + (valuey[entry] * math.sin(math.radians(angle)))))
    return valuex, valuey

### Prevents wrong data by comparing the length of the list
def preventwrongdata(x, y):
    if len(x) != len(y):
        print("Error - data is wrong")
        exit()

### normalises the forcevalues with surface and fluid speed and density
def normi (convunit, surface_normi):
    v_normi= awslist[-1]
    roh_norm=1.2041 #kg/m^3
    for entry in range(len(convunit)):
        convunit[entry]= abs((( convunit[entry]/(0.5*roh_norm*math.pow(v_normi, 2)*surface_normi) )))
    return convunit


#clean up a value from the brackets and tranform from scientific to float
def cleanvalues(value):
    value= str(value.replace("((", ""))
    value= float(value)
    return value
 
### clean up and make time an int while rotating 
#   the koord.system and normalize data if necessary
def writetolist(fx, fy, time):
    for entry in range(len(fx)):
        fx[entry] = cleanvalues(fx[entry])
        fy[entry] = cleanvalues(fy[entry])
        time[entry]=int(time[entry])
    return fx, fy, time
 
### summs the input values together
def sumvalues3(value1, value2, value3):
    sumvalue = []
    for entry in range(len(value1)):
        if addhull_switch:
            pass
        else:
            value3[entry]=0
      
        sumvalue.append(value1[entry] + value2[entry] + value3[entry] ) 
    return sumvalue

### summs the input values together
def sumvalues4(sail1force, sail2force, sail3force, sail4force):
    sumvalue = []
    for entry in range(len(sail1force)):
        if addhull_switch:
            pass
        else:
            sail4force[entry]=0
        sumvalue.append(sail1force[entry] + sail2force[entry] + sail3force[entry] + sail4force[entry] ) 
    return sumvalue

# def localwrite(data):
#     ii = 0
#     time = []
#     fx = []
#     fy = []
#     for line in open(data, 'r'):
#         if ii <= 2: # prevent that the first few lines will be read
#             ii = ii + 1
#             continue
#         lines = [i for i in line.split()]
#         time.append((lines[0]))
#         fx.append((lines[1]))
#         fy.append((lines[2])) 
#     return (fx, fy, time)

### read the locations inputed as a string of the remotehost 
#   and write it to lists
def remotewrite(data, remotehost):
    ii = 0
    time = []
    fx = []
    fy = []
    with Connection(remotehost, user) as c, c.sftp() as sftp,   \
            sftp.open(data) as filee:
        for line in filee:
 
            if ii <= 2: # prevent that the first few lines will be read
                ii = ii + 1
                continue
            lines = [i for i in line.split()]
            time.append((lines[0]))
            fx.append((lines[1]))
            fy.append((lines[2])) 
    return (fx, fy, time)   


# def remotewritelastvalue(data, remotehost):
#     ii = 0
#     time = []
#     fx = []
#     fy = []
#     c= Connection(remotehost, user)
#     sftp= c.sftp()
#     filee= sftp.open(data)
#     last_line = filee.readlines()[-1]
#     lines = [i for i in last_line.split()]
#     time.append((lines[0]))
#     fx.append((lines[1]))
#     fy.append((lines[2]))    
#     # with Connection(remotehost, user) as c, c.sftp() as sftp,   \
#     #         sftp.open(data) as filee:
#     #     last_line = filee.readlines()[-1]
#     #     lines = [i for i in last_line.split()]
#     #     time.append((lines[0]))
#     #     fx.append((lines[1]))
#     #     fy.append((lines[2]))
#     #c.close()
#     return (fx, fy, time) 

### read the locations inputed as a string of the remotehost 
#   and write the last value to a variable
def remotewritelastvalue(data, remotehost):
    ii = 0
    time = []
    fx = []
    fy = []
    with Connection(remotehost, user) as c, c.sftp() as sftp,   \
            sftp.open(data) as filee:
        last_line = filee.readlines()[-1]
        lines = [i for i in last_line.split()]
        time.append((lines[0]))
        fx.append((lines[1]))
        fy.append((lines[2])) 
    return (fx, fy, time) 

### Initial reading of the locations of the force files at the 
#   remote serveras input for later reading 
def remotedataread(remotehost, case):
    global sailcount
    sailcount= int(case.split("/")[-1][0]) # cut the first number from the case string to identify number of sails

    with Connection(remotehost, user) as c, c.sftp() as sftp:
            output= c.run('find '+case+' -type f -name forces.dat', warn=True)
            output= output.stdout.split("\n")[:-1]
            
            if sailcount == 2:

                for iii in range(len(output)):
                    if "bowsailforces" in output[iii]:
                        sail1data.append(output[iii])
                    elif "sternsailforces" in output[iii]:
                        sail2data.append(output[iii])
                    elif "hullforces" in output[iii]:
                        hullforcedata.append(output[iii])
            elif sailcount == 3:
                for iii in range(len(output)):
                    if "sail1" in output[iii]:
                        sail1data.append(output[iii])
                    elif "sail2" in output[iii]:
                        sail2data.append(output[iii])
                    elif "sail3" in output[iii]:
                        sail3data.append(output[iii])                        
                    elif "hull" in output[iii]:
                        hullforcedata.append(output[iii])

            else:
                print("error with filename input")
                exit()            
            c.close()

### clears the values after a case is run through
def clearvars():
    sail1data.clear()
    sail2data.clear()
    sail3data.clear()    
    hullforcedata.clear()
    simutime.clear()
    listforce_x_sail1.clear()
    listforce_y_sail1.clear()
    listforce_x_sail2.clear()
    listforce_y_sail2.clear()
    listforce_x_sail3.clear()
    listforce_y_sail3.clear()    
    force_x_sum.clear()
    force_y_sum.clear()
    caselist.clear()
    awslist.clear()
    awalist.clear()
    aoasail1list.clear()
    aoasail2list.clear()
    aoasail3list.clear()


### MAINRUN
def mainrun(rangee, host):
    
    # Check if the files have the same length
    preventwrongdata(sail1data, sail2data)

    for file in range(rangee):
        
        time_sail1 = []
        fx_sail1 = []
        fy_sail1 = []
        time_sail2 = []
        fx_sail2 = []
        fy_sail2 = []  
        time_sail3 = []
        fx_sail3 = []
        fy_sail3 = []          
        time_hull = []
        fx_hull = []
        fy_hull = []
        fx_sum= []
        fy_sum= []
    
        if sailcount == 3:
            degsail1 = sail1data[file].split("/")[-8]
            degsail2 = sail1data[file].split("/")[-7]
            degsail3 = sail1data[file].split("/")[-6]
        else:
            degsail1 = [k for k in sail1data[file].split("/") if 'degbow' in k][0][:-6]
            degsail2 = [k for k in sail1data[file].split("/") if 'degstern' in k][0][:-8]
        vel = [k for k in sail1data[file].split("/") if 'v_' in k][0][2:]
        global awa
        awa = [k for k in sail1data[file].split("/") if 'deg' in k][0][:-3]
        awslist.append(int(vel))
        awalist.append(int(awa))
        aoasail1list.append(int(degsail1))
        aoasail2list.append(int(degsail2))
        if sailcount == 3:
            aoasail3list.append(int(degsail3))
    
        # single case name define
        caselist[file] = ("vel"+vel+"_awa"+awa+"_degsail1"+degsail1+"_degsail2"+degsail2)

        # initial write out of data
        fx_sail1, fy_sail1, time_sail1 = remotewritelastvalue(sail1data[file], host)
        writetolist(fx_sail1, fy_sail1, time_sail1)
        fx_sail2, fy_sail2, time_sail2 = remotewritelastvalue(sail2data[file], host)
        writetolist(fx_sail2, fy_sail2, time_sail2)   
        fx_hull, fy_hull, time_hull = remotewritelastvalue(hullforcedata[file], host)
        writetolist(fx_hull, fy_hull, time_hull)
        # conversion and tranforming the data
        if sailcount == 3:
            fx_sail3, fy_sail3, time_sail3 = remotewritelastvalue(sail3data[file], host)
            writetolist(fx_sail3, fy_sail3, time_sail3)   
            fx_sum= sumvalues4(fx_sail1, fx_sail2, fx_sail3, fx_hull)
            fy_sum= sumvalues4(fy_sail1, fy_sail2, fy_sail3, fy_hull) 
        else:
            fx_sum= sumvalues3(fx_sail1, fx_sail2, fx_hull)
            fy_sum= sumvalues3(fy_sail1, fy_sail2, fy_hull)

        if rotated:
            rotatex(fx_sail1, fy_sail1)
            rotatex(fx_sail2, fy_sail2)
            if sailcount== 3:
                rotatex(fx_sail3, fy_sail3)
            rotatex(fx_sum, fy_sum)    
        if norm:
            if sailcount== 3:
                for count in (fx_sail1, fy_sail1, fx_sail2, fy_sail2, fx_sail3, fy_sail3):
                    count= normi(count, surface)
                for count_sum in (fx_sum, fy_sum):
                        count_sum= normi(count_sum, (((3*surface))+hulllateralsurface))
            else:
                for count in (fx_sail1, fy_sail1, fx_sail2, fy_sail2):
                    count= normi(count, surface)                
                for count_sum in (fx_sum, fy_sum):
                        count_sum= normi(count_sum, (((2*surface))+hulllateralsurface))

        listforce_x_sail1[file]= fx_sail1
        listforce_y_sail1[file]= fy_sail1
        listforce_x_sail2[file]= fx_sail2
        listforce_y_sail2[file]= fy_sail2
        listforce_x_sail3[file]= fx_sail3
        listforce_y_sail3[file]= fy_sail3
        force_x_sum[file]= fx_sum     
        force_y_sum[file]= fy_sum 



def plotpolarsingle(forcex, forcey, aws, awa_l, aoas1, aoas2, aoas3, vel, windangle, farbe, label_plot, case, caseslist_local):
    n=0 # counter for legend
    sail= case
    def doplot(variant):
        plt.legend(loc="best")
        plt.grid(True,linestyle = '--')
        plt.annotate('s1=aoasail1\ns2=aoasail2\ns3=aoasail3', xy=(0.15, 0.5),xycoords='axes fraction', fontsize=5, horizontalalignment='right', \
            verticalalignment='bottom', bbox=dict(facecolor='white'))        
        if ((rotated != True) and (norm)):

            plt.title(sail+'s '+variant+"x/"+variant+"y "+str(vel)+" m/s - awa="+str(windangle)+"°")
            plt.ylabel(variant+'y')
            plt.xlabel(variant+'x')
            plt.savefig('/home/daniel/innocalc/plots/'+naming+'/'+sail+'_'+variant+'x_'+variant+'y_'+str(vel)+"_ms_"+str(windangle)+"°", \
                dpi=300,bbox_inches='tight')
        elif rotated:
            plt.ylabel(variant+'l')
            plt.xlabel(variant+'d')
            plt.title(sail+'s '+variant+"d/"+variant+"l "+str(vel)+" m/s - awa="+str(windangle)+"°")
            plt.savefig('/home/daniel/innocalc/plots/'+naming+'/'+sail+'_'+variant+'d_'+variant+'l_'+str(vel)+"_ms_"+str(windangle)+"°", \
                dpi=300,bbox_inches='tight')
        else:
            plt.ylabel(variant+'y in N')
            plt.xlabel(variant+'x in N')
            plt.title(sail+'s '+variant+"x/"+variant+"y "+str(vel)+" m/s - awa="+str(windangle)+"°")
            plt.savefig('/home/daniel/innocalc/plots/'+naming+'/'+sail+'_'+variant+'x_'+variant+'y_'+str(vel)+"_ms_"+str(windangle)+"°", \
                dpi=300,bbox_inches='tight')



    for caseno in range(len(caseslist_local)):
        if aws[caseno] == vel:
                if abs(force_x_sum[caseno][0]) <= 20000000: # filter obviously wrong cases!
                    if (awa_l[caseno]) == windangle:
                        if n == 0:
                            plt.plot(forcex[caseno][0], forcey[caseno][0], '-ok', color=farbe, label=label_plot)
                            n=1
                        else:
                            plt.plot(forcex[caseno][0], forcey[caseno][0], '-ok', color=farbe)
                        if farbe== 'r':

                            plt.text(forcex[caseno][0]+(forcex[caseno][0]/200), forcey[caseno][0], \
                                        " s1:"+str(aoas1[caseno])+"|s2:"+str(aoas2[caseno])\
                                        +"|s3:"+str(aoas3[caseno]),fontsize=5)
                        else:
                            plt.text(forcex[caseno][0]+(forcex[caseno][0]/200), forcey[caseno][0], \
                                        " s1:"+str(aoas1[caseno])+"|s2:"+str(aoas2[caseno]),fontsize=5)                        
    if norm:
        doplot('C')
    else:
        doplot('F')

def makesimpletimeforcesplot(which,rota_true):
    fig,ax=plt.subplots()
    timeplot = simutime[which][cutvales:]
    forcexplot_bow = listforce_x_sail1[which][cutvales:]
    forceyplot_bow = listforce_y_sail1[which][cutvales:]
    forcexplot_stern = listforce_x_sail2[which][cutvales:]
    forceyplot_stern = listforce_y_sail2[which][cutvales:]
    if sternforces_switch:
        ax.plot(timeplot, forcexplot_stern, color='salmon',  label='F_x_stern')
    if bowforces_switch:
        ax.plot(timeplot, forcexplot_bow, color='r',  label='F_x_bow')    
    if sumforces_switch:
        forcexsum_plot = force_x_sum[which][cutvales:]
        forceysum_plot = force_y_sum[which][cutvales:]
        ax.plot(timeplot, forcexsum_plot, color='sienna',  label='F_x_both')    

    
    ax.set_ylabel('Fx in N', color="red")
    # plt.text(int(len(timeplot)*0.9), int(max(forcexplot_bow)*0.5), statistics.median(forcexplot_bow),
    #     horizontalalignment='center',
    #     verticalalignment='center')
    ax.set_title(caselist[which])

    # prevent error when no data with shrinkage
    if len(timeplot)/10 == 0:
        ax.text(4, 0, 'data not valid ', fontsize = 22, bbox = dict(facecolor = 'red', alpha = 0.5))
        ax.set_xticks(simutime[which], rotation=90)
    else:
        #ax.set_xticks(timeplot[::int(len(timeplot)/10)], rotation=90)
        print("nice")
    ax2=ax.twinx() # second axis
    if sternforces_switch:
        ax2.plot(timeplot, forceyplot_stern, color='lime', label='F_y_stern')
    if bowforces_switch:
        ax2.plot(timeplot, forceyplot_bow, color='g', label='F_y_bow')
    if sumforces_switch:
        ax2.plot(timeplot, forceysum_plot, color='aquamarine', label='F_y_both')


    ax2.set_ylabel('Fy in N', color="green")  

    lines, labels = ax.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax2.legend(lines + lines2, labels + labels2, loc='upper left')
    
    #plt.gcf().autofmt_xdate()
    #name_plot = "graph" + str(which) + ".svg" #LATE for good SVG
    #name_plot = "graph" + str(which) + ".png"
    #plt.savefig(name_plot)
    plt.grid()
    if rota_true== 'true':
        plt.savefig('plots/'+case+'rotated/'+caselist[which], dpi=300,bbox_inches='tight')
    else:
        plt.savefig('plots/'+case+'/'+caselist[which], dpi=300,bbox_inches='tight')#
    plt.close()

# for i in range(len(caselist)):
#     makesimpletimeforcesplot(i,rotated)






# surface=case1sailsurface # set the right surface
# remotedataread(host1, case1)
# mainrun(len(sail1data), host1)
# plotpolarsingle(10, 30,'r', case1.split("/")[-1])

# clearvars()

# surface=case2sailsurface
# remotedataread(host2, case2)
# mainrun(len(sail1data), host2)
# plotpolar(10, 30,'g', case2.split("/")[-1]) # 2mastdyna

deg= 60, 90

surface=case1sailsurface # set the right surface
remotedataread(host1, case1) #3Mastdyna
mainrun(len(sail1data), host1)
    
sail1data_case2 = copy.deepcopy(sail1data)
sail2data_case2 = copy.deepcopy(sail2data)
sail3data_case2 = copy.deepcopy(sail3data)

hullforcedata_case2 = copy.deepcopy(hullforcedata)
simutime_case2 = copy.deepcopy(simutime)
listforce_x_sail1_case2 = copy.deepcopy(listforce_x_sail1)
listforce_y_sail1_case2 = copy.deepcopy(listforce_y_sail1)
listforce_x_sail2_case2 = copy.deepcopy(listforce_x_sail2)
listforce_y_sail2_case2 = copy.deepcopy(listforce_y_sail2)
listforce_x_sail3_case2 = copy.deepcopy(listforce_x_sail3)
listforce_y_sail3_case2 = copy.deepcopy(listforce_y_sail3)

force_x_sum_case2 = copy.deepcopy(force_x_sum)
force_y_sum_case2 = copy.deepcopy(force_y_sum)
caselist_case2 = copy.deepcopy(caselist)
awslist_case2 = copy.deepcopy(awslist)
awalist_case2 = copy.deepcopy(awalist)
aoasail1list_case2 = copy.deepcopy(aoasail1list)
aoasail2list_case2 = copy.deepcopy(aoasail2list)
aoasail3list_case2 = copy.deepcopy(aoasail3list)
# _case2 = 3MastDyna
clearvars()

surface=case2sailsurface 
remotedataread(host2, case2)
mainrun(len(sail1data), host2)
for n in deg:
    
    # plotpolarsingle(listforce_x_sail1_case2, listforce_y_sail1_case2, awslist_case2, awalist_case2, \
    #     aoasail1list_case2, aoasail2list_case2, aoasail3list_case2, 10, n,'r', case1.split("/")[-1], 'Sail1', caselist_case2)
    # plotpolarsingle(listforce_x_sail1, listforce_y_sail1, awslist, awalist, \
    #     aoasail1list, aoasail2list, aoasail3list, 10, n,'g', case2.split("/")[-1], 'Sail1', caselist)
    # plt.close()
    # plt.clf()
    # plotpolarsingle(listforce_x_sail2_case2, listforce_y_sail2_case2, awslist_case2, awalist_case2, \
    #     aoasail1list_case2, aoasail2list_case2, aoasail3list_case2, 10, n,'r', case1.split("/")[-1], 'Sail2', caselist_case2)
    # plotpolarsingle(listforce_x_sail2, listforce_y_sail2, awslist, awalist, \
    #     aoasail1list, aoasail2list, aoasail3list, 10, n,'g', case2.split("/")[-1], 'Sail2', caselist)    
    # plt.close()    
    # plt.clf()
    # plotpolarsingle(listforce_x_sail3_case2, listforce_y_sail3_case2, awslist_case2, awalist_case2, \
    #     aoasail1list_case2, aoasail2list_case2, aoasail3list_case2, 10, n,'r', case1.split("/")[-1], 'Sail3', caselist_case2)
    # # plotpolarsingle(listforce_x_sail3, listforce_y_sail3, awslist, awalist, \
    # #     aoasail1list, aoasail2list, aoasail3list, 10, n,'g', case1.split("/")[-1], 'Sail3', caselist)
    # plt.close()    
    # plt.clf()
    plotpolarsingle(force_x_sum_case2, force_y_sum_case2, awslist_case2, awalist_case2, \
        aoasail1list_case2, aoasail2list_case2, aoasail3list_case2, 10, n,'r', case1.split("/")[-1], 'sum', caselist_case2)
    plotpolarsingle(force_x_sum, force_y_sum, awslist, awalist, \
        aoasail1list, aoasail2list, aoasail3list, 10, n,'g', case2.split("/")[-1], 'sum', caselist)

    plt.close()    
    plt.clf()
